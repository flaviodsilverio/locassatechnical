
APIManager - The manager (hence the name) of the API calls, the API calls are using a simple NSURLSession.

CustomAnnotation - Just a view with pre-configured labels to show the required info. Additionally it contains a delegate for the refresh button, this was unnecessary since we could just add the target on the creation of the view, still I do like this style of approaches hence I made it.

Map Controller - The main view controller