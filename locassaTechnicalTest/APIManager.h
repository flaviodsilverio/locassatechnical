//
//  APIManager.h
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 24/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol APIManagerDelegate <NSObject>

@optional
- (void) requestCompletedSuccessfully:(NSDictionary *)data;
- (void) requestProblem;

@end

@interface APIManager : NSObject

@property (weak) id <APIManagerDelegate> delegate;

- (NSInteger) getTemperatureForCity:(NSString *)city inCountry:(NSString *)country;


@end
