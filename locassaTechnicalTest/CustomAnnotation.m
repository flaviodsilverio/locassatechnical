//
//  CustomAnnotation.m
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 25/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import "CustomAnnotation.h"

@implementation CustomAnnotation
{
    NSTimer *timer;
}
@synthesize locationLabel, temperatureLabel, refreshButton, loadingIndicator;

- (id) initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setBackgroundColor:[UIColor lightGrayColor]];
        [[self layer] setCornerRadius:10];
        
        CGSize labelsSize = CGSizeMake(self.frame.size.width - 20, self.frame.size.height / 3);
        
        locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, labelsSize.width, labelsSize.height)];
        [locationLabel setFont:[UIFont systemFontOfSize:18]];
        [locationLabel setTextAlignment:NSTextAlignmentCenter];
        [locationLabel setTextColor:[UIColor whiteColor]];
        [self addSubview:locationLabel];
        
        temperatureLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, labelsSize.height, labelsSize.width, labelsSize.height)];
        [temperatureLabel setTextAlignment:NSTextAlignmentCenter];
        [temperatureLabel setTextColor:[UIColor whiteColor]];
        [temperatureLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [self addSubview:temperatureLabel];
        
        refreshButton = [[UIButton alloc] initWithFrame:CGRectMake(10,  labelsSize.height * 2, labelsSize.width, labelsSize.height)];
        [[refreshButton titleLabel] setTextAlignment:NSTextAlignmentCenter];
        [refreshButton addTarget:self action:@selector(refreshTemperature:) forControlEvents:UIControlEventTouchUpInside];
        [[refreshButton titleLabel] setFont:[UIFont systemFontOfSize:18]];
        [refreshButton setTitleColor:[UIColor colorWithRed:.196 green:0.3098 blue:0.52 alpha:1.0] forState:UIControlStateNormal];
        [self addSubview:refreshButton];
        
        loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(10, 0, labelsSize.width, self.frame.size.height)];
        [loadingIndicator setHidden:YES];
        [self addSubview:loadingIndicator];
    }
    
    return self;
}

- (void) setLoading:(BOOL)active{

    if(active){
    
        [loadingIndicator setHidden:NO];
        [refreshButton setHidden:YES];
        [temperatureLabel setHidden:YES];
        [locationLabel setHidden:YES];
        [loadingIndicator startAnimating];
        timer = [NSTimer scheduledTimerWithTimeInterval: 5.0
                                                target: self
                                              selector:@selector(timeOut)
                                              userInfo:nil
                                               repeats:NO];
    } else {
    
        [loadingIndicator setHidden:YES];
        [refreshButton setHidden:NO];
        [temperatureLabel setHidden:NO];
        [locationLabel setHidden:NO];
        [loadingIndicator stopAnimating];
        
        [timer invalidate];
        timer = nil;
    }
}

- (void) timeOut{
    [locationLabel setText:@"Error"];
    [temperatureLabel setText:@"Please Refresh"];
    [self setLoading:NO];
}

#pragma mark - Delegate

- (void) refreshTemperature:(id) sender {

    if([[self delegate] respondsToSelector:@selector(refreshTemperature:)]){
        [[self delegate] refreshTemperature:sender];
    }
    
}

@end
