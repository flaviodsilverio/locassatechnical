//
//  MapController.h
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 24/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "APIManager.h"
#import "CustomAnnotation.h"

@interface MapController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, APIManagerDelegate, CustomAnnotationDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) CLLocationManager *locationManager;


@end
