//
//  MapController.m
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 24/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import "MapController.h"


@interface MapController ()

@property (strong, nonatomic) NSString *localityName, *countryName;

@end

@implementation MapController
{
    CustomAnnotation *currentLocation;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {

    if(!_locationManager) {
    
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
        [_locationManager requestWhenInUseAuthorization];
        _locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        [_locationManager startUpdatingLocation];
        
    }

}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {

    currentLocation = [[CustomAnnotation alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];

    [currentLocation.refreshButton setTitle:@"Refresh" forState:UIControlStateNormal];
    [currentLocation setDelegate:self];
    [currentLocation setLoading:TRUE];
    
    return currentLocation;
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{

    if(![[_locationManager location] isEqual:[locations lastObject]]){
        
        [_mapView setCenterCoordinate:[[_locationManager location] coordinate] animated:YES];
        [self cityAndCountryNamesForLocation:[_locationManager location]];
        
    
    }

    
}

- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{

    if(status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways){
        
        [_locationManager startUpdatingLocation];

    }

}

#pragma mark - Utilities

- (void)cityAndCountryNamesForLocation:(CLLocation *)newLocation{

    CLGeocoder * geoCoder = [CLGeocoder new];
    
    [geoCoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       for (CLPlacemark *placemark in placemarks) {
                           
                           APIManager *api = [APIManager new];
                           [api setDelegate:self];
                           self.localityName = [placemark locality];
                           [api getTemperatureForCity:_localityName inCountry:[placemark country]];
        
                       }
                   }];
}

#pragma mark - API Manager delegation methods

- (void) requestCompletedSuccessfully:(NSDictionary *)data {

    [currentLocation setLoading:NO];
    [currentLocation.temperatureLabel setText:[NSString stringWithFormat:@"%@, %@°",[data valueForKeyPath:@"query.results.channel.item.condition.text"],[data valueForKeyPath:@"query.results.channel.item.condition.temp"]]];
    [currentLocation.locationLabel setText:_localityName];
    
    NSInteger temperature = [[data valueForKeyPath:@"query.results.channel.item.condition.temp"] integerValue];
    
    if(temperature < 0){
    
        [self.view setBackgroundColor:[UIColor colorWithRed:0 green:191 blue:255 alpha:1]];
        
    } else if(temperature >= 0 && temperature < 10) {
    
        [self.view setBackgroundColor:[UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:200.0/255.0 alpha:1.0]];

    } else if(temperature >= 10 && temperature < 20) {
    
        [self.view setBackgroundColor:[UIColor grayColor]];
        
    } else if(temperature >= 20 && temperature < 30) {
    
        [self.view setBackgroundColor:[UIColor orangeColor]];
        
    } else {
    
        [self.view setBackgroundColor:[UIColor redColor]];
    }
    
}

- (void) requestProblem {

    [currentLocation.locationLabel setText:@"Error"];
    [currentLocation.temperatureLabel setText:@"Please Refresh"];
    [currentLocation setLoading:NO];
    
}

#pragma mark - Custom Annotation Delegate

- (void) refreshTemperature:(id) sender{

    [currentLocation setLoading:YES];
    [self cityAndCountryNamesForLocation:[_locationManager location]];

}


@end
