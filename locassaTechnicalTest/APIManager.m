//
//  APIManager.m
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 24/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import "APIManager.h"

@implementation APIManager



- (NSInteger) getTemperatureForCity:(NSString *)city inCountry:(NSString *)country{
    
    NSString *query = [NSString stringWithFormat:@"select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%@, %@\") and u='c'", city, country];

    NSString *dataUrl = [NSString stringWithFormat:@"https://query.yahooapis.com/v1/public/yql?q=%@&format=json&env=store%%3A%%2F%%2Fdatatables.org%%2Falltableswithkeys",[query stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
  //  NSString *dataUrl = [NSString stringWithFormat:@"http://where.yahooapis.com/geocode?location=%f,%f&flags=J&gflags=R&appid=zHgnBS4m", latitude, longitude];

    NSURL *url = [NSURL URLWithString:dataUrl];

    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if([[response valueForKey:@"statusCode"]  isEqual:@200]){
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"%@",json);
            
            [self requestCompletedSuccessfully:json];
            
        } else {
            
            [self requestProblem];

        }
    }];
    
    [dataTask resume];



    return 10;

}

- (void) requestCompletedSuccessfully:(NSDictionary *)data{
    
    if([[self delegate] respondsToSelector:@selector(requestCompletedSuccessfully:)]){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[self delegate] requestCompletedSuccessfully:data];
        });
    }
}
- (void) requestProblem{
    
    if([[self delegate] respondsToSelector:@selector(requestProblem)]){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[self delegate] requestProblem];
        });
        
    }
}

@end
