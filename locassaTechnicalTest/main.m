//
//  main.m
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 24/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
