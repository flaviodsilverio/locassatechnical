//
//  AppDelegate.h
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 24/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

