//
//  CustomAnnotation.h
//  locassaTechnicalTest
//
//  Created by Flávio Silvério on 25/02/16.
//  Copyright © 2016 Flávio Silvério. All rights reserved.
//

#import <MapKit/MapKit.h>

@protocol CustomAnnotationDelegate <NSObject>

@optional
- (void) refreshTemperature:(id) sender;

@end

@interface CustomAnnotation : MKAnnotationView

@property (weak) id <CustomAnnotationDelegate> delegate;

@property (strong, nonatomic) UILabel *locationLabel;
@property (strong, nonatomic) UILabel *temperatureLabel;
@property (strong, nonatomic) UIButton *refreshButton;

@property (strong, nonatomic) UIActivityIndicatorView *loadingIndicator;


- (void) setLoading:(BOOL)active;

@end
